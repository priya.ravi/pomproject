package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ClickLeadPage extends ProjectMethods{

	public ClickLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH, using="//*[@id=\"label\"]/a") WebElement ClickCRM;
	@FindBy(how = How.XPATH, using="//*[@id=\"ext-gen41\"]/ul/li[2]/div/div/div/div/div/a") WebElement ClickLead; 
	
	public ClickLeadPage ClickCRM() {
		//WebElement eleUsername = locateElement("id", "username");
		click(ClickCRM);
		return this;
	}
		
	public ClickLeadPage ClickLead() {
		//WebElement eleUsername = locateElement("id", "username");
		click(ClickLead);
		return this; 
	}
}













