package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ClickMyLeadPage extends ProjectMethods{

	public ClickMyLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.CLASS_NAME,using="selected") WebElement ClickMyLeads;
	
	public ClickMyLeadPage enterUsername() {
		//WebElement eleUsername = locateElement("id", "username");
		click(ClickMyLeads);
		return this; 
	}
}













