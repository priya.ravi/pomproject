package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ClickCreateLeadPage extends ProjectMethods{

	public ClickCreateLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement cname;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement enterfirstName;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement enterlastName;
	@FindBy(how = How.NAME,using = "Create Lead") WebElement createLead;
	public ClickCreateLeadPage entercname() {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(cname, "Indium");
		return this; 
	}
	public ClickCreateLeadPage enterfirstName() {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(enterfirstName, "Priya");
		return this;
	}
	public ClickCreateLeadPage enterlastName() {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(enterlastName, "R");
		return this;
	}
	public HomePage createLead() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(createLead);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new HomePage();
	}
}













