package com.framework.testcases1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods{

	@BeforeTest
    	public void setData() {
		testCaseName = "TC001_LoginLogout";
		testDescription = "Login into leaftaps";
		testNodes = "Leads";
		author = "Priya";
		category = "smoke";
		dataSheetName = "TC_001";
	}
	@Test (dataProvider = "fetchData")
		public void login(String username, String password) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin();
		}
	}

